CC			= gcc

LIBNAME		= png_manager
LIB_SYS		= /usr/lib/$(LIBNAME)

OUTDIR		= lib
HEADER_DIR	= headers
HEADER_SYS	= /usr/include/$(LIBNAME)

HEADERS		= 
HEADERS		+= png_manager.h

BINARY		= $(OUTDIR)/libpngm.a

# ############################################################################

CFLAGS		=
CFLAGS		+= -O2 -fPIC

INCLUDES	=
INCLUDES	+= -I .

LIBRARIES	=
LIBRARIES	+= -lm -lpng

# ############################################################################

SOURCES		= $(shell find . -name '*.c'| grep -e ".*.c")
OBJECTS		= $(SOURCES:%.c=$(OUTDIR)/%.o)

# ############################################################################

all: directories headers $(BINARY)

$(BINARY): $(OBJECTS)
	ar rcs $@ $^

$(OBJECTS): $(OUTDIR)/%.o: %.c
	mkdir -p $(@D)
	$(CC) $(INCLUDES) $(CFLAGS) -c $< -o $@
	
# ############################################################################

.PHONY: directories
directories:
	mkdir -p $(OUTDIR)
	mkdir -p $(HEADER_DIR)

.PHONY: headers
headers:
	cp $(HEADERS) $(HEADER_DIR)

.PHONY: install
install:
	mkdir -p $(HEADER_SYS)
	cp $(HEADERS) $(HEADER_SYS)
	mkdir -p $(LIB_SYS)
	cp $(BINARY) $(LIB_SYS)

.PHONY: uninstall
uninstall:
	rm -rf $(HEADER_SYS)
	rm -rf $(LIB_SYS)

.PHONY: clean
clean:
	rm -rf $(OUTDIR)
	rm -rf $(HEADER_DIR)
