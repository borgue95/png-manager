#include "png_manager.h"

#include <stdlib.h>
#include <string.h>


struct pngm *read_png(const char *filename)
{
    struct pngm *pngm = pngm_init_for_reading(filename);
    pngm_read_header(pngm);
    pngm_read_bytes(pngm);
    pngm_read_end(pngm);
    return pngm;
}

void write_png_gray(uint8_t *data, int w, int h, const char *filename)
{
    struct pngm *pngm = pngm_init_for_writing(filename);
    pngm_write_header_gray(pngm, w, h);
    pngm_write_bytes_gray(pngm, data);
    pngm_write_end(pngm);
}

void write_png_color(uint8_t *data, int w, int h, int color_type, const char *filename)
{
    struct pngm *pngm = pngm_init_for_writing(filename);
    pngm->row_size = w * color_type;
    if (color_type == PNGM_RGBA) {
        pngm->color_type = PNG_COLOR_TYPE_RGBA;
    } else {
        pngm->color_type = PNG_COLOR_TYPE_RGB;
    }
    pngm_write_header_color(pngm, w, h);
    pngm_write_bytes_color(pngm, data);
    pngm_write_end(pngm);
}

struct pngm *pngm_init_for_reading(const char *filename)
{
    // init my struct
    struct pngm* pngm = malloc(sizeof(struct pngm));
    if (!pngm) {
        printf("Error initializing png output. Lack of memory?\n");
        exit(EXIT_FAILURE);
    }

    // open file
    pngm->fp = fopen(filename, "rb");
    if (!pngm->fp) {
        printf("Error opening png file for reading. Correct path?\n");
        exit(EXIT_FAILURE);
    }

    // check if it is a png file
    char header[8];
    fread(header, 1, 8, pngm->fp);
    if (png_sig_cmp(header, 0, 8)) {
        printf("File %s is not recognised as a png file\n", filename);
        exit(EXIT_FAILURE);
    }


    /* initialize stuff */
    pngm->png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!pngm->png_ptr) {
        printf("Error creating png struct\n");
        exit(EXIT_FAILURE);
    }

    // init more png lib
    pngm->info_ptr = png_create_info_struct(pngm->png_ptr);
    if (!pngm->info_ptr) {
        png_destroy_write_struct(&pngm->png_ptr, NULL);
        printf("Error creating png info\n");
        exit(EXIT_FAILURE);
    }

    // init file inside png lib
    if (setjmp(png_jmpbuf(pngm->png_ptr))) {
        printf("[read_png_file] Error during init_io\n");
        exit(EXIT_FAILURE);
    }
    png_init_io(pngm->png_ptr, pngm->fp);

    return pngm;
}

struct pngm *pngm_init_for_writing(const char *filename)
{
    // init my struct
    struct pngm* pngm = malloc(sizeof(struct pngm));
    if (!pngm) {
        printf("Error initializing png output. Lack of memory?\n");
        exit(EXIT_FAILURE);
    }

    // open file
    pngm->fp = fopen(filename, "wb");
    if (!pngm->fp) {
        printf("Error opening png file for writing\n");
        exit(EXIT_FAILURE);
    }

    // init png lib
    pngm->png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, 
                                            NULL, NULL, NULL);
    if (!pngm->png_ptr) {
        printf("Error creating png struct\n");
        exit(EXIT_FAILURE);
    }

    // init more png lib
    pngm->info_ptr = png_create_info_struct(pngm->png_ptr);
    if (!pngm->info_ptr) {
        png_destroy_write_struct(&pngm->png_ptr, NULL);
        printf("Error creating png info\n");
        exit(EXIT_FAILURE);
    }

    // init file inside png lib
    if (setjmp(png_jmpbuf(pngm->png_ptr))) {
        printf("[write_png_file] Error during init_i\n");
        exit(EXIT_FAILURE);
    }
    png_init_io(pngm->png_ptr, pngm->fp);

    return pngm;
}

void pngm_read_header(struct pngm *pngm)
{
    png_set_sig_bytes(pngm->png_ptr, 8);
    png_read_info(pngm->png_ptr, pngm->info_ptr);

    pngm->w = png_get_image_width(pngm->png_ptr, pngm->info_ptr);
    pngm->h = png_get_image_height(pngm->png_ptr, pngm->info_ptr);
    png_byte color_type = png_get_color_type(pngm->png_ptr, pngm->info_ptr);
    png_byte bit_depth = png_get_bit_depth(pngm->png_ptr, pngm->info_ptr);

    if (color_type == PNG_COLOR_TYPE_RGB) {
        pngm->color_type = PNGM_RGB;
    } else if (color_type == PNG_COLOR_TYPE_RGBA) {
        pngm->color_type = PNGM_RGBA;
    } else if (color_type == PNG_COLOR_TYPE_GRAY) {
        pngm->color_type = PNGM_GRAY;
    } else {
        printf("Color type of input png image not supported\n");
        exit(EXIT_FAILURE);
    }

    pngm->row_size = pngm->w * pngm->color_type;

    if (bit_depth != 8) {
        printf("Bit depth of input png image not supported\n");
        exit(EXIT_FAILURE);
    }

    // ?
    int number_of_passes = png_set_interlace_handling(pngm->png_ptr);
    png_read_update_info(pngm->png_ptr, pngm->info_ptr);
}

void pngm_write_header_gray(struct pngm *pngm, int w, int h)
{
    // write header 
    if (setjmp(png_jmpbuf(pngm->png_ptr))) {
        printf("[write_png_file] Error during writing header\n");
        exit(EXIT_FAILURE);
    }
    png_set_IHDR(pngm->png_ptr, 
                 pngm->info_ptr, 
                 w, 
                 h,
                 8, // bit_depth
                 PNG_COLOR_TYPE_GRAY,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);
    png_write_info(pngm->png_ptr, pngm->info_ptr);

    pngm->w = w;
    pngm->h = h;
}

void pngm_write_header_color(struct pngm *pngm, int w, int h)
{
    // write header 
    if (setjmp(png_jmpbuf(pngm->png_ptr))) {
        printf("[write_png_file] Error during writing header\n");
        exit(EXIT_FAILURE);
    }
    png_set_IHDR(pngm->png_ptr, 
                 pngm->info_ptr, 
                 w, 
                 h,
                 8, // bit_depth
                 pngm->color_type,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);
    png_write_info(pngm->png_ptr, pngm->info_ptr);

    pngm->w = w;
    pngm->h = h;
}

void pngm_read_bytes(struct pngm *pngm)
{
    /* read file */
    if (setjmp(png_jmpbuf(pngm->png_ptr))) {
        printf("[read_png_file] Error reading bytes\n");
        exit(EXIT_FAILURE);
    }

    pngm->row_pointers = (uint8_t**) malloc(sizeof(png_bytep) * pngm->h);
    for (int y = 0; y < pngm->h; y++)
        pngm->row_pointers[y] = (uint8_t*) malloc(png_get_rowbytes(pngm->png_ptr,pngm->info_ptr));

    png_read_image(pngm->png_ptr, pngm->row_pointers);
}

void pngm_write_bytes_gray(struct pngm *pngm, uint8_t *data)
{
    // transform data to row_pointers
    uint8_t **row_pointers = malloc(sizeof(uint8_t*) * pngm->h);
    pngm->row_pointers = row_pointers; // for later destruction
    if (!row_pointers) {
        printf("Not enough memory for row_pointers\n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < pngm->h; i++) {
        row_pointers[i] = malloc(sizeof(uint8_t) * pngm->w);
        if (!row_pointers[i]) {
            printf("Not enough memory for row_pointers\n");
            exit(EXIT_FAILURE);
        }
        memcpy(row_pointers[i], data + i*pngm->h, pngm->w);
    }

    // write bytes
    if (setjmp(png_jmpbuf(pngm->png_ptr))) {
        printf("[write_png_file] Error during writing bytes\n");
        exit(EXIT_FAILURE);
    }
    png_write_image(pngm->png_ptr, row_pointers);

}

void pngm_write_bytes_color(struct pngm *pngm, uint8_t *data)
{
    // transform data to row_pointers
    uint8_t **row_pointers = malloc(sizeof(uint8_t*) * pngm->h);
    pngm->row_pointers = row_pointers; // for later destruction
    if (!row_pointers) {
        printf("Not enough memory for row_pointers\n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < pngm->h; i++) {
        row_pointers[i] = malloc(sizeof(uint8_t) * pngm->row_size);
        if (!row_pointers[i]) {
            printf("Not enough memory for row_pointers\n");
            exit(EXIT_FAILURE);
        }
        memcpy(row_pointers[i], data + i * pngm->row_size, pngm->row_size);
    }

    // write bytes
    if (setjmp(png_jmpbuf(pngm->png_ptr))) {
        printf("[write_png_file] Error during writing bytes\n");
        exit(EXIT_FAILURE);
    }
    png_write_image(pngm->png_ptr, row_pointers);

}

void pngm_read_end(struct pngm *pngm)
{
    // end reading
    if (setjmp(png_jmpbuf(pngm->png_ptr))) {
        printf("[read_png_file] Error during end of read\n");
        exit(EXIT_FAILURE);
    }
    png_read_end(pngm->png_ptr, NULL);
    png_destroy_read_struct(&pngm->png_ptr, &pngm->info_ptr, NULL);
}

void pngm_write_end(struct pngm *pngm)
{
    // end writing
    if (setjmp(png_jmpbuf(pngm->png_ptr))) {
        printf("[write_png_file] Error during end of write\n");
        exit(EXIT_FAILURE);
    }
    png_write_end(pngm->png_ptr, NULL);
    png_destroy_write_struct(&pngm->png_ptr, &pngm->info_ptr);
    pngm_destroy(pngm);
}

void pngm_destroy(struct pngm *pngm)
{
    // cleanup row_pointers
    for (int i = 0; i < pngm->h; i++) {
        free(pngm->row_pointers[i]);
    }
    free(pngm->row_pointers);

    fclose(pngm->fp);

    // destroy myself
    free(pngm);
}

