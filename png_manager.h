#ifndef PNG_MANAGER_H_
#define PNG_MANAGER_H_

#include <stdint.h>
#include <stdio.h>
#include <png.h>

#define PNGM_RGB  3
#define PNGM_RGBA 4
#define PNGM_GRAY 1

struct pngm {
    FILE *fp;
    png_structp png_ptr;
    png_infop info_ptr;
    uint8_t **row_pointers;
    int w, h;
    int color_type;
    int bit_depth;
    int row_size;
};    

// public
struct pngm* read_png(const char *filename);
void write_png_gray(uint8_t *data, int w, int h, const char *filename);
void write_png_color(uint8_t *data, int w, int h, int color_type, const char *filename);

// private
struct pngm *pngm_init_for_reading(const char *filename);
struct pngm *pngm_init_for_writing(const char *filename);

void pngm_read_header(struct pngm *pngm);
void pngm_write_header_gray(struct pngm *pngm, int w, int h);
void pngm_write_header_color(struct pngm *pngm, int w, int h);

void pngm_read_bytes(struct pngm *pngm);
void pngm_write_bytes_gray(struct pngm *pngm, uint8_t *data);
void pngm_write_bytes_color(struct pngm *pngm, uint8_t *data);

void pngm_read_end(struct pngm *pngm);
void pngm_write_end(struct pngm *pngm);

void pngm_destroy(struct pngm *pngm);

#endif
