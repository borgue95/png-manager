# PNG Manager

## Description

PNG Manager is a simple interface to interact with the PNG library (`libpng`) present in most GNU/Linux distros written in C to use in C/C++ programs. It wraps the following features:

* Read a PNG file. 
    * **Color depth**: 8 bits.
    * **Color types**: `RGB`, `RGBA`, and `GRAY`. I've successfully worked with packed pixels. 

* Write a PNG file.
    * **Color depth**: 8 bits.
    * **Color types**: `RGB`, `RGBA`, and `GRAY`. I've successfully worked with packed pixels. 

I know that is a very limited API, but it serves the reading and writing of the most common PNG files in a simple way. 

## Internals

All the information in packed on the `pngm` struct. Here are the fields:

* `fp`. Contains the file descriptor. It is used internally. 
* `png_ptr`. It is the pointer to the PNG data from the `libpng` library. It is used internally. 
* `info_ptr`. It contains additional information about the PNG image. This is a pointer used by `libpng`. 
* `row_pointers`. By default, `libpng` returns the data in a `uint8_t` format in a dynamic matrix. Id est, every row has its chunck of memory and `row_pointers` is a dynamic array which contains the pointers to all rows. 
* `w` and `h`. Are the width and the height of the image. 
* `color_type`. It is the color type: `RGB` is defined as 3, `RGBA` is defined as 4 and `GRAY` is defined as 1. This way, this field also indicates the size of each pixel in bytes.
* `row_size`. Contains the size of the row. It is usually the number of pixels multiplied by the size of each pixel. 

### Reading a PNG file

It as simple as:

```c
struct pngm *pngm = read_png(filename);
```

With this call, the fields `w`, `h`, `color_type`, `row_size` and `row_pointers` become available to the user for reading. 

Remember that the format of `row_pointers` are this (in case of `RGBA` color type):

```
row_pointers = [pointer_to_row_0, pointer_to_row_1, ..., pointer_to_row_h-1]
```

```
pointer_to_row_i = [r0,g0,b0,a0,r1,b1,g1,a1, ..., r_w-1,g_w-1,b_w-1,a_w-1]
```

Once `row_pointers` are no longer needed, you can free'd them using the destroy function:

```c
pngm_destroy(pngm);
```

### Writing a PNG file

The only way to write a PNG image for now is to have it on a single dynamically allocated vector of type `uint8_t`. I may add new writing methods in the future. Also, I may add a more simplified way to write images. Call this function to write a color PNG image:

```c
write_png_color(vector_to_write, width, height, color_type, filename);
```

The color type must be `PNGM_RGB` or `PNGM_RGBA`. To write a `GRAY` image, call this other one:

```c
write_png_gray(vector_to_write, width, height, color_type, filename);
```

with `color_type = PNGM_GRAY`. Those functions assume that there is enought elements in the vector to satisfy the width, height and color type parameters. 

## Installation

I develop a simple `Makefile` to compile and install this library to the system. I may improve this with more flexibility in the future. 

```bash
git clone https://gitlab.com/borgue95/png-manager.git
cd png-manager
make
sudo make install
```

There is no requirements to compile this library, but to use it, you must have `libpng` installed on your system. 




